﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RownanieKwadratowe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RownanieKwadratowe.Tests
{

    [TestClass()]
    public class Class1Tests
    {
        private Class1 _class1;

        [TestInitialize]
        public void Init() => _class1 = new Class1();



        [TestMethod()]
        public void SolveTest()
        {
            double a = 1;
            double b = 4;
            double c = 3;
            Tuple<double, double> t = Tuple.Create(-1.0, -3.0);
            

            Assert.AreEqual(t,_class1.Solve(a,b,c));
        }
    }
}