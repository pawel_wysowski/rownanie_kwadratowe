﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RownanieKwadratowe
{
    public class Class1
    {
       public  double x1 = 0;
       public double x2 = 0;
       public double x0 = 0;
       public string NoSolution = "ZNALEZIONO ROZWIĄZANIA";
        public Tuple<double, double> Solve(double a, double b, double c)
        {

            double d = 0;



            d = b * b - 4 * a * c;
            if (d > 0)
            {
                x1 = (-b + Math.Sqrt(d)) / 2 * a;
                x2 = (-b - Math.Sqrt(d)) / 2 * a;
                return Tuple.Create(x1, x2);
            }
            else if (d == 0)
            {

                x1 = -b + Math.Sqrt(d) / 2 * a;
                x2 = x1;
                return Tuple.Create(x1, x2);
            }
            else
                return null;
                


        }

    }
}
