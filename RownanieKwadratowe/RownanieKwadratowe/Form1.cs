﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RownanieKwadratowe
{
    public partial class Form1 : Form
    {
        Class1 c;
        public Form1()
        {
            InitializeComponent();
            c = new Class1();
        }


       
        private void button1_Click(object sender, EventArgs e)
        {
            textBox4.Clear();
            textBox5.Clear();
            var x = c.Solve(Double.Parse(textBox1.Text), Double.Parse(textBox2.Text), Double.Parse(textBox3.Text));

            textBox4.Text = x.Item1.ToString();
            textBox5.Text = x.Item2.ToString();
            MessageBox.Show(c.NoSolution);
        }
    }
}
